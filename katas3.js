const content = document.querySelector('#content');

const sampleArray = [
  469,
  755,
  244,
  245,
  758,
  450,
  302,
  20,
  712,
  71,
  456,
  21,
  398,
  339,
  882,
  848,
  179,
  535,
  940,
  472
];

function kata1() {
  let k1 = document.createElement('p');
  for (let i = 1; i <= 25; i++) {
    k1.textContent += i + ' ';
    content.appendChild(k1);
  }
}
kata1();

function kata2() {
  let k2 = document.createElement('p');
  for (let i = 25; i >= 1; i--) {
    k2.textContent += i + ' ';
    content.appendChild(k2);
  }
}
kata2();

function kata3() {
  let k3 = document.createElement('p');
  for (let i = -1; i >= -25; i--) {
    k3.textContent += i + ' ';
    content.appendChild(k3);
  }
}
kata3();

function kata4() {
  let k4 = document.createElement('p');
  for (let i = -25; i <= -1; i++) {
    k4.textContent += i + ' ';
    content.appendChild(k4);
  }
}
kata4();

function kata5() {
  let k5 = document.createElement('p');
  for (let i = 25; i >= -25; i--) {
    if (i % 2 === 1 || i % 2 === -1) {
      k5.textContent += i + ' ';
      content.appendChild(k5);
    }
  }
}
kata5();

function kata6() {
  let k6 = document.createElement('p');
  for (let i = 3; i <= 100; i++) {
    if (i % 3 === 0) {
      k6.textContent += i + ' ';
      content.appendChild(k6);
    }
  }
}
kata6();

function kata7() {
  let k7 = document.createElement('p');
  for (let i = 7; i <= 100; i++) {
    if (i % 7 === 0) {
      k7.textContent += i + ' ';
      content.appendChild(k7);
    }
  }
}
kata7();

function kata8() {
  let k8 = document.createElement('p');
  for (let i = 100; i >= 3; i--) {
    if (i % 7 === 0 || i % 3 === 0) {
      k8.textContent += i + ' ';
      content.appendChild(k8);
    }
  }
}
kata8();

function kata9() {
  let k9 = document.createElement('p');
  for (let i = 5; i <= 100; i++) {
    if (i % 5 === 0 && i % 2 === 1) {
      k9.textContent += i + ' ';
      content.appendChild(k9);
    }
  }
}
kata9();

function kata10(sampleArray) {
  let k10 = document.createElement('p');
  for (let i = 0; i < sampleArray.length; i++) {
    k10.textContent += sampleArray[i] + ' ';
    content.appendChild(k10);
  }
}
kata10(sampleArray);

function kata11(sampleArray) {
  let k11 = document.createElement('p');
  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] % 2 === 0) {
      k11.textContent += sampleArray[i] + ' ';
      content.appendChild(k11);
    }
  }
}
kata11(sampleArray);

function kata12(sampleArray) {
  let k12 = document.createElement('p');
  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] % 2 === 1) {
      k12.textContent += sampleArray[i] + ' ';
      content.appendChild(k12);
    }
  }
}
kata12(sampleArray);

function kata13(sampleArray) {
  let k13 = document.createElement('p');
  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] % 8 === 0) {
      k13.textContent += sampleArray[i] + ' ';
      content.appendChild(k13);
    }
  }
}
kata13(sampleArray);

function kata14(sampleArray) {
  let k14 = document.createElement('p');
  let quad = 0;
  for (let i = 0; i < sampleArray.length; i++) {
    quad = sampleArray[i] ** 2;
    k14.textContent += quad + ' ';
    content.appendChild(k14);
  }
}
kata14(sampleArray);

function kata15(sum) {
  let k15 = document.createElement('p');
  sum = (20 * (20 + 1)) / 2;
  k15.textContent = sum;
  content.appendChild(k15);
}
kata15();

function kata16(sampleArray) {
  let k16 = document.createElement('p');
  k16.textContent = sampleArray.reduce((a, b) => a + b, 0);
  content.appendChild(k16);
}
kata16(sampleArray);

function kata17(sampleArray) {
  let k17 = document.createElement('p');
  k17.textContent = Math.min.apply(Math, sampleArray);
  content.appendChild(k17);
}
kata17(sampleArray);

function kata18(sampleArray) {
  let k18 = document.createElement('p');
  k18.textContent = Math.max.apply(Math, sampleArray);
  content.appendChild(k18);
}
kata18(sampleArray);

function createRectangle(w) {
  let rec1 = document.createElement('div');
  rec1.className = 'rec1';
  document.body.appendChild(rec1);
  rec1.style.width = w;
}

function rectangle() {
  for (let i = 0; i <= 20; i++) {
    let n = 100 + 'px';
    createRectangle(n);
  }
}
rectangle();

function rectangle2() {
  for (let i = 0; i <= 20; i++) {
    let n = 105 + i * 5 + 'px';
    createRectangle(n);
  }
}
rectangle2();

function rectangle3(sampleArray) {
  for (let i = 0; i <= sampleArray.length; i++) {
    let n = sampleArray[i] + 'px';
    createRectangle(n);
  }
}
rectangle3(sampleArray);

function rectangle4(sampleArray) {
  for (let i = 0; i <= sampleArray.length; i++) {
    let n = sampleArray[i] + 'px';
    const rec2 = document.createElement('div');
    rec2.className = 'rec2';
    document.body.appendChild(rec2);
    rec2.style.width = n;
    if (i % 2 === 0) {
      rec2.style.backgroundColor = 'red';
    }
  }
}
rectangle4(sampleArray);

function rectangle5(sampleArray) {
  for (let i = 0; i <= sampleArray.length; i++) {
    let n = sampleArray[i] + 'px';
    const rec2 = document.createElement('div');
    rec2.className = 'rec2';
    document.body.appendChild(rec2);
    rec2.style.width = n;
    if (sampleArray[i] % 2 === 0) {
      rec2.style.backgroundColor = 'red';
    }
  }
}
rectangle5(sampleArray);
